# UI

Simple UI is served at the index page of application, like `http://localhost:3000/`.

This page displays all projects and it's ecosystems currently handled by the application.

It allows to synchronize state with GitLab via `sync` button and also to manually trigger dependency update run via `execute` button.

## Authentication

By default application allows anonymous access. To disable anonymous access, environment variable `SETTINGS__ANONYMOUS_ACCESS`
should be set to `false`.

### Creating users

In order to access projects page when anonymous access is disabled, a user must be created. Currently user can be created via
[create_user rake](./rake.md#create-user) task. Once a user has been created, it can be used to authenticate and access projects page.
