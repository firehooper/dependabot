# ======================================================================================================================
# Global
# ======================================================================================================================
stages:
  - build
  - static analysis
  - test
  - report
  - release
  - deploy

default:
  image: registry.gitlab.com/dependabot-gitlab/ci-images:ruby
  interruptible: true

variables:
  # App tags
  CURRENT_TAG: ${CI_COMMIT_REF_SLUG}-${CI_COMMIT_SHORT_SHA}
  # Docker images
  APP_IMAGE: ${CI_REGISTRY_IMAGE}/dev:${CURRENT_TAG}
  MOCK_IMAGE: thiht/smocker:0.18.0
  QEMU_IMAGE: tonistiigi/binfmt:qemu-v7.0.0
  # Misc
  DOCKER_VERSION: "23.0"
  CODACY_VERSION: "13.10.7"
  DEPENDENCY_SCANNING_DISABLED: "true"
  SAST_DISABLED: "true"
  CODE_QUALITY_DISABLED: "true"
  LICENSE_MANAGEMENT_DISABLED: "true"
  BUILD_PLATFORM: linux/amd64,linux/arm64
  LARGE_RUNNER_TAG: docker

include:
  - local: .gitlab/ci/jobs.gitlab-ci.yml
  - local: .gitlab/ci/rules.gitlab-ci.yml

# ======================================================================================================================
# Pre Stage
# ======================================================================================================================
dont-interrupt:
  extends:
    - .dont-interrupt
    - .rules:dont-interrupt

cache-dependencies:
  extends:
    - .cache_dependencies
    - .rules:main

# ======================================================================================================================
# Build Stage
# ======================================================================================================================
compile-assets:
  extends:
    - .compile_assets
    - .rules:main
  needs: [cache-dependencies]

build-core-image:
  extends:
    - .build_core_image
    - .rules:build-core-image

build-app-image:
  extends:
    - .build_app_image
    - .rules:build-image
  needs:
    - job: build-core-image
      optional: true
    - job: compile-assets
      artifacts: true

build-docs:
  extends:
    - .build_docs
    - .rules:build-docs

# ======================================================================================================================
# Static analysis stage
# ======================================================================================================================
rubocop:
  extends:
    - .rubocop
    - .rules:main
  needs: [cache-dependencies]

reek:
  extends:
    - .reek
    - .rules:main
  needs: [cache-dependencies]

brakeman:
  extends:
    - .brakeman
    - .rules:main
  needs: []

dependency-scan:
  extends:
    - .dependency_scan
    - .rules:dependency-scan
  needs: []

license-scan:
  extends:
    - .license_scan
    - .rules:license-scan
  needs: []

# ======================================================================================================================
# Test Stage
# ======================================================================================================================
unit-test-standalone:
  extends:
    - .unit-test-standalone
    - .rules:main
  needs: [cache-dependencies]

unit-test-service:
  extends:
    - .unit-test-service
    - .rules:main
  needs: [cache-dependencies]

system-test:
  extends:
    - .system-test
    - .rules:main
  needs: [cache-dependencies]

e2e-test-standalone-run:
  extends:
    - .standalone-test
    - .rules:main
  needs: [build-app-image]

e2e-test-deployment-docker:
  extends:
    - .deploy-test
    - .rules:main
  needs: [build-app-image]

migration-test:
  extends:
    - .migration-test
    - .rules:migration-test
  needs: [build-app-image]

# ======================================================================================================================
# Reporting
# ======================================================================================================================
publish-coverage:
  extends:
    - .coverage
    - .rules:coverage
  needs:
    - cache-dependencies
    - unit-test-standalone
    - unit-test-service
    - system-test

# ======================================================================================================================
# Release Stage
# ======================================================================================================================
release-image:
  extends:
    - .release_image
    - .rules:release
  dependencies: []

create-gitlab-release:
  extends:
    - .gitlab_release
    - .rules:release
  needs: [release-image]

update-helm-chart:
  extends:
    - .update_chart
    - .rules:release
  needs: [release-image]

update-standalone-repo:
  extends:
    - .update_standalone
    - .rules:release
  needs: [release-image]

pages:
  extends:
    - .publish_docs
    - .rules:release-docs
  dependencies: [build-docs]

# ======================================================================================================================
# Deploy Stage
# ======================================================================================================================
deploy:
  extends:
    - .deploy
    - .rules:deploy
