# frozen_string_literal: true

class LogEntryTtlIndex < Mongoid::Migration
  def self.up
    if Update::Run.index_specification(created_at: 1)
      Update::Run.collection.database.command({ dropIndexes: "update_runs", index: "created_at_1" })
    end

    Update::LogEntry.create_indexes
    Update::LogEntry.where(:timestamp.lt => Time.zone.now - AppConfig.expire_run_data).delete
    Update::LogEntry.all.update_all("$set" => { created_at: Time.zone.now }) # rubocop:disable Rails/SkipsModelValidations
  end

  def self.down
    Update::Run.create_indexes
    Update::LogEntry.collection.database.command({ dropIndexes: "update_log_entries", index: "created_at_1" })
  end
end
