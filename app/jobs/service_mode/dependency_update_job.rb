# frozen_string_literal: true

module ServiceMode
  module DependencyUpdateJob
    # Perform dependency updates and merge request creation
    #
    # @param [Hash] args
    # @return [Array]
    def perform(args)
      super
    ensure
      save_execution_details
    end

    # Execute dependency updates
    #
    # @return [void]
    def execute(&block)
      create_update_run

      super
    end

    # Persist execution errors
    #
    # @return [void]
    def save_execution_details
      update_run.save_errors!(UpdateFailures.fetch)
      update_run.save_log_entries!(UpdateLogs.fetch)
    end
  end
end
