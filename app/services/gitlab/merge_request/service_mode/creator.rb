# frozen_string_literal: true

module Gitlab
  module MergeRequest
    module ServiceMode
      module Creator
        # Create merge request
        #
        # @return [Gitlab::ObjectifiedHash]
        def call
          @mr = super
          return unless mr

          persist_mr
          close_superseded_mrs
          mr
        end

        private

        attr_reader :mr

        # Persist merge request
        #
        # @return [void]
        def persist_mr
          ::MergeRequest.create!(
            project: project,
            id: mr.id,
            iid: mr.iid,
            package_ecosystem: config_entry[:package_ecosystem],
            directory: config_entry[:directory],
            state: "opened",
            auto_merge: updated_dependency.auto_mergeable?,
            squash: squash?,
            update_from: updated_dependency.previous_versions,
            update_to: updated_dependency.current_versions,
            main_dependency: updated_dependency.name,
            branch: gitlab_creator.branch_name,
            target_branch: fetcher.source.branch,
            commit_message: gitlab_creator.commit_message,
            target_project_id: target_project_id
          )
        end

        # Close superseded merge requests
        #
        # @return [void]
        def close_superseded_mrs
          superseded_mrs.each do |superseded_mr|
            superseded_mr.close
            BranchRemover.call(project.name, superseded_mr.branch)
            Commenter.call(
              target_project_id || project.name,
              superseded_mr.iid,
              "This merge request has been superseded by #{mr.web_url}"
            )
          end
        end

        # List of open superseded merge requests
        #
        # @return [Mongoid::Criteria]
        def superseded_mrs
          project.superseded_mrs(
            update_from: updated_dependency.previous_versions,
            package_ecosystem: config_entry[:package_ecosystem],
            directory: config_entry[:directory],
            mr_iid: mr.iid
          )
        end

        # MR message footer with available commands
        #
        # @return [String]
        def message_footer
          return unless AppConfig.integrated?

          <<~MSG
            ---

            <details>
            <summary>Dependabot commands</summary>
            <br />
            You can trigger Dependabot actions by commenting on this MR

            - `#{AppConfig.commands_prefix} rebase` will rebase this MR
            - `#{AppConfig.commands_prefix} recreate` will recreate this MR rewriting all the manual changes and resolving conflicts

            </details>
          MSG
        end
      end
    end
  end
end
