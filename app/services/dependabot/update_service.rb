# frozen_string_literal: true

module Dependabot
  class TooManyRequestsError < StandardError
    def message
      "GitHub API rate limit exceeded! See: https://docs.github.com/en/rest/overview/resources-in-the-rest-api#rate-limiting"
    end
  end

  class ExternalCodeExecutionError < StandardError
    def initialize(package_ecosystem, directory)
      super(<<~ERR)
        Unexpected external code execution detected.
        Option 'insecure-external-code-execution' must be set to 'allow' for entry:
          package_ecosystem - '#{package_ecosystem}'
          directory - '#{directory}'
      ERR
    end
  end

  # Main entrypoint class for updating dependencies and creating merge requests
  #
  class UpdateService < UpdateBase
    def initialize(args)
      super(args[:project_name])

      @package_ecosystem = args[:package_ecosystem]
      @directory = args[:directory]
      @dependency_name = args[:dependency_name]
    end

    # Create or update mr's for dependencies
    #
    # @return [void]
    def call
      init_gitlab

      update
    rescue Octokit::TooManyRequests
      raise TooManyRequestsError
    rescue Dependabot::UnexpectedExternalCode
      raise ExternalCodeExecutionError.new(package_ecosystem, directory)
    ensure
      FileUtils.rm_r(repo_contents_path, force: true, secure: true) if repo_contents_path
    end

    private

    attr_reader :package_ecosystem, :directory, :dependency_name

    # Project
    #
    # @return [Project]
    def project
      @project ||= fetch_config.then do |config|
        fork_id = config_entry(config)[:fork] ? gitlab.project(project_name).to_h.dig("forked_from_project", "id") : nil
        Project.new(name: project_name, configuration: config, forked_from_id: fork_id)
      end
    end

    # Open mr limits
    #
    # @return [Number]
    def limits
      @limits ||= {
        mr: config_entry[:open_merge_requests_limit],
        security_mr: config_entry[:open_security_merge_requests_limit]
      }
    end

    # Update type, security fix or dependency bump
    #
    # @param [Dependabot::Dependencies::UpdatedDependency] dependency
    # @return [Symbol]
    def update_type(dependency)
      dependency.vulnerable ? :security_mr : :mr
    end

    # Project configuration fetched from gitlab
    #
    # @return [Config]
    def fetch_config
      Config::Fetcher.call(project_name)
    end

    # Update project dependencies
    #
    # @return [void]
    def update
      dependencies.each_with_object({ mr: Set.new, security_mr: Set.new }) do |dep, count|
        # updating dependency is using Dir.chdir so thread needs to be locked
        updated_dep = Semaphore.synchronize { updated_dependency(dep) }
        next if update_dependency(updated_dep, count) # go to next dep if mr was created or updated

        handle_vulnerability_issues(updated_dep)
        close_obsolete_mrs(updated_dep.name)
      end
    end

    # Create dependency update merge request
    #
    # @param [Dependabot::Dependencies::UpdatedDependency] dependency
    # @param [Hash] mrs
    # @return [void]
    def update_dependency(dependency, mrs)
      type = update_type(dependency)

      return unless dependency.updates?
      return unless create_mr?(mrs, type)

      iid = MergeRequest::CreateService.call(
        project: project,
        fetcher: fetcher,
        config_entry: config_entry,
        updated_dependency: dependency,
        credentials: credentials
      )&.iid

      mrs[type] << iid if iid
    end

    # Handle vulnerability issues
    #
    # @param [Dependabot::Dependencies::UpdatedDependency] updated_dep
    # @return [void]
    def handle_vulnerability_issues(_updated_dep); end

    # Close obsolete merge requests
    #
    # @param [String] dependency_name
    # @return [void]
    def close_obsolete_mrs(_dependency_name); end

    # Check if mr should be created based on limits settings
    #
    # @param [Hash] mrs
    # @param [Symbol] type
    # @return [Boolean]
    def create_mr?(mrs, type)
      limit = limits[type]

      return true if limit.negative?
      return true if !limit.zero? && (mrs[type].length < limit)

      dep_type = type == :mr ? "dependency" : "vulnerable dependency"
      reason = if limits[type].zero?
                 "due to disabled mr creation setting!"
               else
                 "due to max open mr limit reached, limit: '#{limit}'!"
               end

      log(:info, "  skipping update of #{dep_type} #{reason}")
      false
    end
  end
end

Dependabot::UpdateService.prepend(Dependabot::ServiceMode::UpdateService) if AppConfig.service_mode?
