# frozen_string_literal: true

class Version
  VERSION = "0.35.1"
end
