# frozen_string_literal: true

class LogEntryExpiryUpdater < ApplicationService
  # Update ttl value for log entries model
  #
  # @return [void]
  def call
    # Return if no log entries exist as it can cause namespace not found error
    return if Update::LogEntry.all.empty?

    index_name = "created_at_1"
    ttl = Update::LogEntry.collection.indexes.get(index_name)["expireAfterSeconds"].to_i
    return log(:debug, "Log entry ttl value has not changed, skipping index update") if ttl == AppConfig.expire_run_data

    log(:info, "Updating log entry expiry setting")
    Update::LogEntry.collection.database.command(
      {
        collMod: "update_log_entries",
        index: {
          name: index_name,
          expireAfterSeconds: AppConfig.expire_run_data
        }
      }
    )
  rescue Mongo::Error::OperationFailure => e
    log_error(e, message_prefix: "Failed to update log entry expiry setting!")
  end
end
